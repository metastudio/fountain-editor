class CreateScripts < ActiveRecord::Migration
  def change
    create_table :scripts do |t|
      t.text :body
      t.text :fountain_body

      t.timestamps null: false
    end
  end
end
