ready = ->

  $('form#new_script, form.edit_script').submit (event) ->
    event.preventDefault()
    text = $('#script_body').val()
    fountain.parse text, (output) ->
      $('#script_fountain_body').val(output.html.script)
    this.submit()

$(document).on('turbolinks:load', ready)
